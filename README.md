# @telosid/onyx-typedefs
Typescript typedefs for ONYX

# TelosID NPM Registry

To access `@telosid/onyx-typedefs` in your projects, run the command below:

```bash
echo @telosid:registry=https://gitlab.com/api/v4/projects/29462567/packages/npm/ >> .npmrc
```

## Install

```bash
npm install @telosid/onyx-typedefs
```

## API

<docgen-index>

* [`exec(...)`](#exec)
* [Interfaces](#interfaces)
* [Enums](#enums)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### exec(...)

```typescript
exec(options: IOnyxConfiguration) => any
```

| Param         | Type                                                              |
| ------------- | ----------------------------------------------------------------- |
| **`options`** | <code><a href="#ionyxconfiguration">IOnyxConfiguration</a></code> |

**Returns:** <code>any</code>

--------------------


### Interfaces


#### IOnyxConfiguration

| Prop                               | Type                                                                        | Description                                                                                                                                                                                                                                                                                                                   | Default                                 | Since |
|------------------------------------| --------------------------------------------------------------------------- |-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------|-------|
| **`action`**                       | <code><a href="#onyxpluginaction">OnyxPluginAction</a></code>               | Specify the <a href="#onyxpluginaction">OnyxPluginAction</a> for the plugin to perform                                                                                                                                                                                                                                        | <code>undefined</code>                  | 1.0.0 |
| **`licenseKey`**                   | <code>string</code>                                                         | Specify the ONYX license key                                                                                                                                                                                                                                                                                                  | <code>undefined</code>                  | 1.0.0 |
| **`returnRawImage`**               | <code>boolean</code>                                                        | Specify whether or not to return the raw image                                                                                                                                                                                                                                                                                | <code>false</code>                      | 1.0.0 |
| **`returnProcessedImage`**         | <code>boolean</code>                                                        | Specify whether or not to return the processed image. This would be the most common image type to use for conversion to WSQ or for matching.                                                                                                                                                                                  | <code>false</code>                      | 1.0.0 |
| **`returnEnhancedImage`**          | <code>boolean</code>                                                        | Specify whether or not to return the enhanced image. This is a special image that works better with certain matching algorithms, but needs special testing to make sure it will match.                                                                                                                                        | <code>false</code>                      | 1.0.0 |
| **`returnSlapImage`**              | <code>boolean</code>                                                        | Specify whether or not to return a slap image. This contains all fingers captured within a single image as specified in the FBI EBTS. Returns a single image in the OnyxResult in the slapImage image.                                                                                                                        | <code>false</code>                      | 2.0.0 |
| **`returnSlapWSQ`**                | <code>boolean</code>                                                        | Specify whether or not to return a slap WSQ data. This contains all fingers captured within a single image as specified in the FBI EBTS. Returns a single WSQ in the OnyxResult in the slapImage image.                                                                                                                       | <code>false</code>                      | 2.0.4 |
| **`shouldBinarizeProcessedImage`** | <code>boolean</code>                                                        | Specify whether or not to return the binarized processed fingerprint image in the OnyxResult.                                                                                                                                                                                                                                 | <code>false</code>                      | 2.0.0 |
| **`returnFullFrameImage`**         | <code>boolean</code>                                                        | Specify whether to return a FullFrame image. This returns an image that is suitable for finger detection in a full frame.                                                                                                                                                                                                     | <code>false</code>                      | 1.0.0 |
| **`fullFrameMaxImageHeight`**      | <code>number</code>                                                         | Specify the maximum height for the FullFrame image to be returned, so for example, if you want a 1920 height image returned, pass in 1920.0f for the value. It will a full frame image resized to 1920 for the maximum height. To get the original height of the full frame, to get full resolution, pass in a value of 1.0f. | <code>1920.0</code>                     | 1.0.0 |
| **`returnWSQ`**                    | <code>boolean</code>                                                        | Specify whether or not to return the WSQ image. This will tell the SDK to return a WSQ image suitable for use in matching and other applications.                                                                                                                                                                             | <code>false</code>                      | 1.0.0 |
| **`returnFingerprintTemplate`**    | <code><a href="#fingerprinttemplatetype">FingerprintTemplateType</a></code> | Specify whether or not the capture task will return the specified FingerprintTemplateType in the OnyxResult The ISO template is the ISO/IEC 19794-4 standard minutiae based template The INNOVATRICS template is a proprietary fingerprint template that is highly scale tolerant and useful for touchless matching           | <code>FingerprintTemplateType.NONE</code> | 1.0.0 |
| **`cropSize`**                     | <code><a href="#ionyxcropsize">IOnyxCropSize</a></code>                     | Specify the crop size for the capture image. This will determine the image dimensions in length and width for the resulting images. This should be set to whatever is required for the matching algorithm that is going to be used.                                                                                           | <code>300x512</code>                    | 1.0.0 |
| **`cropFactor`**                   | <code>number</code>                                                         | Specify the crop factor for the capture image. This has a built in scaling factor and this crop factor should be adjusted until you get an image that looks close to images that are known to match against whatever biometric system you are attempting to match against.                                                    | <code>true</code>                       | 1.0.0 |
| **`showLoadingSpinner`**           | <code>boolean</code>                                                        | Specify that the Onyx spinner should be shown during OnyxConfiguration and during image processing.                                                                                                                                                                                                                           | <code>true</code>                       | 1.0.0 |
| **`useManualCapture`**             | <code>boolean</code>                                                        | Specify the method of capture to be a manual capture of the fingerprint.                                                                                                                                                                                                                                                      | <code>false</code>                      | 1.0.0 |
| **`manualCaptureText`**            | <code>string</code>                                                         | Specify the manual capture text that is displayed on screen.                                                                                                                                                                                                                                                                  |                                         | 1.0.0 |
| **`captureFingersText`**           | <code>string</code>                                                         | Specify the name of the text that is displayed on screen to indicate to the user that they should hold their fingers steady, or keep them still during the capture. If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText, and thumbNotInFocusText all have to be populated.                |                                         | 2.0.2 |
| **`captureThumbText`**             | <code>string</code>                                                         | Specify the name of the text that is displayed on screen to indicate to the user that they should hold their thumb steady, or keep it still during the capture. If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText, and thumbNotInFocusText all have to be populated.                    |                                         | 2.0.2 |
| **`fingersNotInFocusText`**        | <code>string</code>                                                         | Specify the name of the text that is displayed on screen to indicate to the user that they should move their fingers until they are in focus. If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText, and thumbNotInFocusText all have to be populated.                                      |                                         | 2.0.2 |
| **`thumbNotInFocusText`**          | <code>string</code>                                                         | Specify the name of the text that is displayed on screen to indicate to the user that they should move their thumb until it is in focus. If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText, and thumbNotInFocusText all have to be populated.                                           |                                         | 2.0.2 |
| **`useOnyxLive`**                  | <code>boolean</code>                                                        | Specify to use ONYX LIVE_FINGER liveness detection as part of the configuration. This will take additional processing time.                                                                                                                                                                                                   | <code>false</code>                      | 1.0.0 |
| **`useFlash`**                     | <code>boolean</code>                                                        | Specify to use the flash                                                                                                                                                                                                                                                                                                      | <code>true</code>                       | 1.0.0 |
| **`reticleOrientation`**           | <code><a href="#onyxreticleorientation">OnyxReticleOrientation</a></code>   | Specify the orientation of the reticle                                                                                                                                                                                                                                                                                        | <code>OnyxReticleOrientation.LEFT</code> | 1.0.0 |
| **`computeNfiqMetrics`**           | <code>boolean</code>                                                        | Specify to compute NFIQ metrics                                                                                                                                                                                                                                                                                               | <code>false</code>                      | 1.0.0 |
| **`targetPixelsPerInch`**          | <code>number</code>                                                         | Specify target pixels per inch                                                                                                                                                                                                                                                                                                |                                         | 2.0.0 |
| **`subjectId`**                    | <code>string</code>                                                         | Specify the unique subject id                                                                                                                                                                                                                                                                                                 |                                         | 2.0.0 |
| **`uploadMetrics`**                | <code>boolean</code>                                                        | For future use. Not currently implemented. Specify whether to upload capture metrics result. This will be used to improve ONYX capture and quality performance in the future.                                                                                                                                                 | <code>true</code>                       | 2.0.0 |
| **`returnOnyxErrorOnLowQuality`**  | <code>boolean</code>                                                        | Specify to return OnyxError on low quality imagery                                                                                                                                                                                                                                                                            | <code>true</code>                       | 2.0.0 |
| **`probe`**                        | <code>string</code>                                                         | Specify the image to probe against the reference image for performing a match.                                                                                                                                                                                                                                                |                                         | 1.0.0 |
| **`reference`**                    | <code>string</code>                                                         | Specify the reference image for performing a match. It is the known image that identifies a person's finger.                                                                                                                                                                                                                  |                                         | 1.0.0 |
| **`pyramidScales`**                | <code>{}</code>                                                             | Specifies the scales for doing image pyramiding. Images will be scaled by the factors specified. For instance, if pass in 0.8, 0.9, 1.1 the images will be scaled to 80%, 90%, and 110%. This is done in order to enhance touchless ONYX fingerpint images to touch-based fingerprint images.                                 |                                         | 1.0.0 |
| **`captureQualityThreshold`**      | <code>number</code>                                                         | Specify the quality threshold to trigger capture. Specify a double value between 0.0 - 1.0 to set the threshold for QualityNet to capture. Higher values mean higher image quality.                                                                                                                                           | <code>0.8</code>                        | 1.1.2 |
| **`fingerDetectionTimeout`**       | <code>number</code>                                                         | Specify the amount of time to wait for successful capture. Specify a value in seconds. Default is 60.0 seconds.                                                                                                                                                                                                                                                           | 2.1.0 |
| **`triggerCaptureOnFingerDetectionTimeout`** | <code>boolean</code>                                              | Trigger a capture of the ONYX preview frame (the screen that shows the finger guide) after the finger detection timeout is reached. If this configuration option is set, then an error will not be thrown when the finger detection timeout is reached, but instead a capture will be initiated. The OnyxResult that is returned may only have the full frame image, (if returnFullFrameImage was set to true). This configuration will also ignore any other settings that might throw an error related to the capture, such as `returnOnyxErrorOnLowQuality` |       |
| **`imageBytes`**                   | <code>string</code>                                                         | ANDROID Only - for now this is a temporary fix to allow Ionic apps to be able to write to the Gallery on Android 10+ devices. Specify the image file bytes to use for saving an image file to the gallery on Android 10 or newer devices.                                                                                     | <code>undefined</code>                  | 1.1.4 |
| **`fileDisplayName`**              | <code>string</code>                                                         | ANDROID Only - for now this is a temporary fix to allow Ionic apps to be able to write to the Gallery on Android 10+ devices. Specify the display file name to use in conjunction with the imageBytes for saving files to the Gallery.                                                                                        | <code>undefined</code>                  | 1.1.4 |


#### IOnyxCropSize

| Prop         | Type                |
| ------------ | ------------------- |
| **`width`**  | <code>number</code> |
| **`height`** | <code>number</code> |


#### IOnyxPluginResult

| Prop              | Type                 |
| ----------------- | -------------------- |
| **`action`**      | <code>string</code>  |
| **`onyxResults`** | <code>{}</code>      |
| **`isVerified`**  | <code>boolean</code> |
| **`matchScore`**  | <code>number</code>  |


#### IOnyxResult

| Prop                                   | Type                                                                |
|----------------------------------------| ------------------------------------------------------------------- |
| **`rawFingerprintDataUri`**            | <code>string</code>                                                 |
| **`processedFingerprintDataUri`**      | <code>string</code>                                                 |
| **`enhancedFingerprintDataUri`**       | <code>string</code>                                                 |
| **`slapImageDataUri`**                 | <code>string</code>                                                 |
| **`base64EncodedSlapWsqBytes`**        | <code>string</code>                                                 |
| **`fullFrameImageDataUri`**            | <code>string</code>                                                 |
| **`base64EncodedFingerprintTemplate`** | <code>string</code>                                                 |
| **`base64EncodedWsqBytes`**            | <code>string</code>                                                 |
| **`captureMetrics`**                   | <code><a href="#ionyxcapturemetrics">IOnyxCaptureMetrics</a></code> |
| **`onyxConfiguration`**                 | <code><a href="#ionyxconfiguration">IOnyxConfiguration</a></code>   |
| **`captureFromFingerDetectionTimeout`** | <code>boolean</code>                                                |


#### IOnyxCaptureMetrics

| Prop                     | Type                                                          |
| ------------------------ | ------------------------------------------------------------- |
| **`nfiqMetrics`**        | <code><a href="#ionyxnfiqmetrics">IOnyxNfiqMetrics</a></code> |
| **`livenessConfidence`** | <code>number</code>                                           |
| **`qualityMetric`**      | <code>number</code>                                           |


#### IOnyxNfiqMetrics

| Prop            | Type                |
| --------------- | ------------------- |
| **`nfiqScore`** | <code>number</code> |


### Enums


#### OnyxPluginAction

| Members                        | Value                                 |
| ------------------------------ | ------------------------------------- |
| **`CAPTURE`**                  | <code>`capture`</code>                |
| **`MATCH`**                    | <code>`match`</code>                  |
| **`REQUEST_FILE_PERMISSIONS`** | <code>`requestFilePermissions`</code> |
| **`WRITE_FILE`**               | <code>`writeFile`</code>              |

#### FingerprintTemplateType

| Members           | Value                      |
| ----------------- | -------------------------- |
| **`NONE`**        | <code>`NONE`</code>        |
| **`ISO`**         | <code>`ISO`</code>         |
| **`INNOVATRICS`** | <code>`INNOVATRICS`</code> |


#### OnyxReticleOrientation

| Members              | Value                         |
| -------------------- | ----------------------------- |
| **`LEFT`**           | <code>`LEFT`</code>           |
| **`RIGHT`**          | <code>`RIGHT`</code>          |
| **`THUMB_PORTRAIT`** | <code>`THUMB_PORTRAIT`</code> |

</docgen-api>
