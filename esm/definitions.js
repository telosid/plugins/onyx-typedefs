export var OnyxPluginAction;
(function (OnyxPluginAction) {
    OnyxPluginAction["CAPTURE"] = "capture";
    OnyxPluginAction["MATCH"] = "match";
    OnyxPluginAction["REQUEST_FILE_PERMISSIONS"] = "requestFilePermissions";
    OnyxPluginAction["WRITE_FILE"] = "writeFile";
})(OnyxPluginAction || (OnyxPluginAction = {}));
export var OnyxReticleOrientation;
(function (OnyxReticleOrientation) {
    OnyxReticleOrientation["LEFT"] = "LEFT";
    OnyxReticleOrientation["RIGHT"] = "RIGHT";
    OnyxReticleOrientation["THUMB_PORTRAIT"] = "THUMB_PORTRAIT";
})(OnyxReticleOrientation || (OnyxReticleOrientation = {}));
export var FingerprintTemplateType;
(function (FingerprintTemplateType) {
    FingerprintTemplateType["NONE"] = "NONE";
    FingerprintTemplateType["ISO"] = "ISO";
    FingerprintTemplateType["INNOVATRICS"] = "INNOVATRICS";
})(FingerprintTemplateType || (FingerprintTemplateType = {}));
//# sourceMappingURL=definitions.js.map