export interface IOnyxPlugin {
    exec(options: IOnyxConfiguration): Promise<IOnyxPluginResult>;
}
export interface IOnyxConfiguration {
    /**
     * Specify the OnyxPluginAction for the plugin to perform
     *
     * @since 1.0.0
     * @default undefined
     */
    action?: OnyxPluginAction;
    /**
     * Specify the ONYX license key
     *
     * @since 1.0.0
     * @default undefined
     */
    licenseKey?: string;
    /**
     Specify whether or not to return the raw image
     *
     * @since 1.0.0
     * @default false
     */
    returnRawImage?: boolean;
    /**
     * Specify whether or not to return the processed image.  This would be the most
     * common image type to use for conversion to WSQ or for matching.
     *
     * @since 1.0.0
     * @default false
     */
    returnProcessedImage?: boolean;
    /**
     * Specify whether or not to return the enhanced image.  This is a special image that
     * works better with certain matching algorithms, but needs special testing to make sure it
     * will match.
     *
     * @since 1.0.0
     * @default false
     */
    returnEnhancedImage?: boolean;
    /**
     * Specify whether or not to return a slap image. This contains all fingers captured
     * within a single image as specified in the FBI EBTS.  Returns a single image in the OnyxResult in the slapImage
     * image.
     *
     * @since 2.0.0
     * @default false
     */
    returnSlapImage?: boolean;
    /**
     * Specify whether or not to return a slap WSQ data. This contains all fingers captured
     * within a single image as specified in the FBI EBTS.  Returns a single WSQ in the OnyxResult in the slapImage
     * image.
     *
     * @since 2.0.4
     * @default false
     */
    returnSlapWSQ?: boolean;
    /**
     * Specify whether or not to return the binarized processed fingerprint image in the OnyxResult.
     *
     * @since 2.0.0
     * @default false
     */
    shouldBinarizeProcessedImage?: boolean;
    /**
     * Specify whether to return a FullFrame image.  This returns
     * an image that is suitable for finger detection in a full frame.
     *
     * @since 1.0.0
     * @default false
     */
    returnFullFrameImage?: boolean;
    /**
     * Specify the maximum height for the FullFrame image to be returned, so for example,
     * if you want a 1920 height image returned, pass in 1920.0f for the value.  It will a full frame
     * image resized to 1920 for the maximum height.  To get the original height of the full frame, to
     * get full resolution, pass in a value of 1.0f.
     *
     * @since 1.0.0
     * @default 1920.0
     */
    fullFrameMaxImageHeight?: number;
    /**
     * Specify whether or not to return the WSQ image.  This will tell the SDK to return
     * a WSQ image suitable for use in matching and other applications.
     *
     * @since 1.0.0
     * @default false
     */
    returnWSQ?: boolean;
    /**
     * Specify whether or not the capture task will return the specified
     * FingerprintTemplateType in the OnyxResult
     * The ISO template is the ISO/IEC 19794-4 standard minutiae based template
     * The INNOVATRICS template is a proprietary fingerprint template that is highly scale tolerant and
     * useful for touchless matching
     *
     * @since 1.0.0
     * @default FingerprintTemplateType.NONE
     */
    returnFingerprintTemplate?: FingerprintTemplateType;
    /**
     * Specify the crop size for the capture image.  This will determine the image dimensions
     * in length and width for the resulting images.  This should be set to whatever is required
     * for the matching algorithm that is going to be used.
     *
     * @since 1.0.0
     * @default 300x512
     */
    cropSize?: IOnyxCropSize;
    /**
     * Specify the crop factor for the capture image.  This has a built in scaling factor and
     * this crop factor should be adjusted until you get an image that looks close to images that are
     * known to match against whatever biometric system you are attempting to match against.
     *
     * @since 1.0.0
     * @default true
     */
    cropFactor?: number;
    /**
     * Specify that the Onyx spinner should be shown during OnyxConfiguration and during
     * image processing.
     *
     * @since 1.0.0
     * @default true
     */
    showLoadingSpinner?: boolean;
    /**
     * Specify the method of capture to be a manual capture of the fingerprint.
     *
     * @since 1.0.0
     * @default false
     */
    useManualCapture?: boolean;
    /**
     * Specify the manual capture text that is displayed on screen.
     *
     * @since 1.0.0
     */
    manualCaptureText?: string;
    /**
     * Specify the name of the text that is displayed on screen to indicate to the user
     * that they should hold their fingers steady, or keep them still during the capture.
     * If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText,
     * and thumbNotInFocusText all have to be populated.
     *
     * @since 2.0.2
     */
    captureFingersText?: string;
    /**
     * Specify the name of the text that is displayed on screen to indicate to the user
     * that they should hold their thumb steady, or keep it still during the capture.
     * If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText,
     * and thumbNotInFocusText all have to be populated.
     *
     * @since 2.0.2
     */
    captureThumbText?: string;
    /**
     * Specify the name of the text that is displayed on screen to indicate to the user
     * that they should move their fingers until they are in focus.
     * If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText,
     * and thumbNotInFocusText all have to be populated.
     *
     * @since 2.0.2
     */
    fingersNotInFocusText?: string;
    /**
     * Specify the name of the text that is displayed on screen to indicate to the user
     * that they should move their thumb until it is in focus.
     * If this field is used, then captureFingersText, captureThumbText, fingersNotInFocusText,
     * and thumbNotInFocusText all have to be populated.
     *
     * @since 2.0.2
     */
    thumbNotInFocusText?: string;
    /**
     * Specify to use ONYX LIVE_FINGER liveness detection as part of the configuration.
     * This will take additional processing time.
     *
     * @since 1.0.0
     * @default false
     */
    useOnyxLive?: boolean;
    /**
     * Specify to use the flash
     *
     * @since 1.0.0
     * @default true
     */
    useFlash?: boolean;
    /**
     * Specify the orientation of the reticle
     *
     * @since 1.0.0
     * @default OnyxReticleOrientation.LEFT
     */
    reticleOrientation?: OnyxReticleOrientation;
    /**
     * Specify to compute NFIQ metrics
     *
     * @since 1.0.0
     * @default false
     */
    computeNfiqMetrics?: boolean;
    /**
     * Specify target pixels per inch
     *
     * @since 2.0.0
     */
    targetPixelsPerInch?: number;
    /**
     * Specify the unique subject id
     *
     * @since 2.0.0
     */
    subjectId?: string;
    /**
     * For future use.  Not currently implemented.  Specify whether to upload capture metrics result.
     * This will be used to improve ONYX capture and quality performance in the future.
     *
     * @since 2.0.0
     * @default true
     * */
    uploadMetrics?: boolean;
    /**
     * Specify to return OnyxError on low quality imagery
     *
     * @since 2.0.0
     * @default true
     * */
    returnOnyxErrorOnLowQuality?: boolean;
    /**
     * Specify the image to probe against the reference image for performing a match.
     *
     * @since 1.0.0
     * */
    probe?: string;
    /**
     * Specify the reference image for performing a match.  It is the known image that identifies a person's finger.
     *
     * @since 1.0.0
     * */
    reference?: string;
    /**
     * Specifies the scales for doing image pyramiding.  Images will be scaled by the factors specified.
     * For instance, if pass in 0.8, 0.9, 1.1 the images will be scaled to 80%, 90%, and 110%.
     * This is done in order to enhance touchless ONYX fingerpint images to touch-based fingerprint images.
     *
     * @since 1.0.0
     * */
    pyramidScales?: string[];
    /**
     * Specify the quality threshold to trigger capture.
     * Specify a double value between 0.0 - 1.0 to set the threshold for QualityNet to capture.
     * Higher values mean higher image quality.
     *
     * @since 1.1.2
     * @default 0.8
     * */
    captureQualityThreshold?: number;
    /**
     * Specify the amount of time to wait for successful capture.
     * Specify a value in seconds.
     * Default is 60.0 seconds.
     * @since 2.1.0
     * @default 60.0
     */
    fingerDetectionTimeout?: number;
    /**
     * Trigger a capture of the ONYX preview frame (the screen that shows the finger guide) after the
     * finger detection timeout is reached.  If this configuration option is set, then an error
     * will not be thrown when the finger detection timeout is reached, but instead a capture
     * will be initiated.  The OnyxResult that is returned may only have the full frame image, (if
     * returnFullFrameImage was set to true).  This configuration will also ignore any other settings
     * that might throw an error related to the capture, such as `returnOnyxErrorOnLowQuality`
     */
    triggerCaptureOnFingerDetectionTimeout?: boolean;
    /**
     * ANDROID Only - for now this is a temporary fix to allow Ionic apps to
     * be able to write to the Gallery on Android 10+ devices.
     * Specify the image file bytes to use for saving an image file to the
     * gallery on Android 10 or newer devices.
     * @since 1.1.4
     * @default undefined
     */
    imageBytes?: string;
    /**
     * ANDROID Only - for now this is a temporary fix to allow Ionic apps to
     * be able to write to the Gallery on Android 10+ devices.
     * Specify the display file name to use in conjunction with the imageBytes
     * for saving files to the Gallery.
     * @since 1.1.4
     * @default undefined
     */
    fileDisplayName?: string;
}
export interface IOnyxCropSize {
    width?: number;
    height?: number;
}
export interface IOnyxPluginResult {
    action?: string;
    onyxResults?: IOnyxResult[];
    isVerified?: boolean;
    matchScore?: number;
}
export interface IOnyxResult {
    rawFingerprintDataUri?: string;
    processedFingerprintDataUri?: string;
    enhancedFingerprintDataUri?: string;
    slapImageDataUri?: string;
    base64EncodedSlapWsqBytes?: string;
    fullFrameImageDataUri?: string;
    base64EncodedFingerprintTemplate?: string;
    base64EncodedWsqBytes?: string;
    captureMetrics?: IOnyxCaptureMetrics;
    onyxConfiguration?: IOnyxConfiguration;
    captureFromFingerDetectionTimeout?: boolean;
}
export interface IOnyxCaptureMetrics {
    nfiqMetrics?: IOnyxNfiqMetrics;
    livenessConfidence?: number;
    qualityMetric?: number;
}
export interface IOnyxNfiqMetrics {
    nfiqScore?: number;
}
export declare enum OnyxPluginAction {
    CAPTURE = "capture",
    MATCH = "match",
    REQUEST_FILE_PERMISSIONS = "requestFilePermissions",
    WRITE_FILE = "writeFile"
}
export declare enum OnyxReticleOrientation {
    LEFT = "LEFT",
    RIGHT = "RIGHT",
    THUMB_PORTRAIT = "THUMB_PORTRAIT"
}
export declare enum FingerprintTemplateType {
    NONE = "NONE",
    ISO = "ISO",
    INNOVATRICS = "INNOVATRICS"
}
